---
title: OnionShare
website: https://onionshare.org/
cover: /files/onionshare.jpg
tags:
  - Dokumentasi & Manajemen Data
categories:
  - Alat Keamanan Digital
  - Berbagi File
  - Perutean Onion
credits: Naskah oleh Khairil Zhafri/EngageMedia.
date: 2023-01-19T17:57:06.460Z
---
OnionShare adaah alat berbagi file yang aman dengan sumber-terbuka dan menggunakan jaringan Tor untuk melindungi data dan privasi pengguna.