---
title: Tails
cover: /files/tails.jpg
website: https://tails.boum.org/
credits: Text by Khairil Zhafri/EngageMedia.
tags:
  - Penghindaran & Anonimitas
categories:
  - Alat Keamanan Digital
  - Perutean Onion
date: 2023-01-19T10:02:41.640Z
---
Tails adalah sistem operasi komputer portabel terbuka yang melindungi penguna dari pengawasan dan membantu mereka menggagalkan penyensoran.