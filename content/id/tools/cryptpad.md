---
title: CryptPad
cover: /files/cryptpad.jpg
website: https://cryptpad.fr/
credits: Text by Khairil Zhafri/EngageMedia.
tags:
  - Aplikasi Web
  - Hosting Mandiri
  - Penelusuran Internet & Produktivitas
categories:
  - Alat Keamanan Digital
  - Penyimpanan File
  - Berbagi File
date: 2023-01-19T21:28:00.311Z
---
CryptPad adalah rangkaian kolaborasi terenkripsi dan terbuka untuk penyimpanan, penyuntingan, dan membagikan lembar sebar, dokumen, presentasi, formulir, dan berkas lainnya secara aman.