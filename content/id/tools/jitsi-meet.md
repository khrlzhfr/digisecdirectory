---
title: Jitsi Meet
cover: /files/jitsimeet.jpg
website: https://jitsi.org/jitsi-meet/
credits: Naskah oleh Khairil Zhafri/EngageMedia.
tags:
  - Komunikasi & Pesan
  - Aplikasi Web
  - Hosting Mandiri
categories:
  - Alat Keamanan Digital
  - Konferensi Video
date: 2023-01-19T10:31:23.647Z
---
Jitsi Meet adalah perangkat lunak konferensi video privat sumber-terbuka yang membatasi sidik jari digital pengguna secara virtual. Jitsi bisa digunakan di sebagian besar peramban internet modern dan juga tersedia dalam bentuk aplikasi seluer untuk Android dan iOS.

> Anda dapat menggunakan [meet.jit.si](https://meet.jit.si/) untuk menyelenggarakan rapat online Anda secara gratis \[…] Server Jitsi Meet gratis tepercaya lainnya termasuk [Greenhost](https://meet.greenhost.net/), [Framatalk](https://framatalk.org/), dan [Distroot](https://calls.disroot.org/). \
> – [Kebersihan Digital 101: Bagaimana menerapkan keamanan dan keselamatan digital](https://engagemedia.org/2022/kesehatan-digital-101-tetap-sehat-melawan-virus-online/?lang=id)