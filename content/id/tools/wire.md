---
title: Wire
cover: /files/wire.jpg
website: https://wire.com/
credits: Naskah oleh Khairil Zhafri/EngageMedia.
flags:
  - Freemium
tags:
  - Komunikasi & Pesan
  - Lintas Platform
categories:
  - Alat Keamanan Digital
  - Obrolan Kolaboratif
date: 2023-01-19T10:45:12.357Z
---
Wire adalah kelengkapan komunikasi grup yang aman dan menawarkan layanan pesan singkat, konferensi video, dan berbagi file yang aman.