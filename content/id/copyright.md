---
type: page
title: Pemberitahuan Hak Cipta
date: 2023-01-30T21:42:36.544Z
---
**digisec.directory** adalah kompilasi referensi keamanan dan keselamatan digital oleh para kontributor [proyek Pelokalan Keamanan Digital EngageMedia](https://engagemedia.org/projects/localization/). 

Semua merek dagang, logo, dan nama merek adalah milik dari pemiliknya masing-masing. Hak cipta semua materi yang dikutip tetap milik penulisnya masing-masing. 

Kecuali ditentukan lain, semua karya lain di situs web ini dilisensikan di bawah [Lisensi Atribusi-NonKomersial 4.0 Internasional Creative Commons](https://creativecommons.org/licenses/by-nc/4.0/legalcode.id). Berdasarkan lisensi ini, Anda bebas untuk:

* Berbagi — menyalin dan menyebarluaskan kembali materi ini dalam bentuk atau format apapun
* Adaptasi — menggubah, mengubah, dan membuat turunan dari materi ini

Kebebasan ini tunduk terhadap ketentuan-ketentuan berikut:

* Atribusi — Anda harus mencantumkan nama yang sesuai, mencantumkan tautan terhadap lisensi, dan menyatakan bahwa telah ada perubahan yang dilakukan. Anda dapat melakukan hal ini dengan cara yang sesuai, namun tidak mengisyaratkan bahwa pemberi lisensi mendukung Anda atau penggunaan Anda.
* NonKomersial — Anda tidak dapat menggunakan materi ini untuk kepentingan komersial.
* Tidak ada pembatasan tambahan — Anda tidak dapat menggunakan ketentuan hukum atau sarana kontrol teknologi yang secara hukum membatasi orang lain untuk melakukan hal-hal yang diizinkan lisensi ini.

Anda tidak perlu menaati lisensi untuk bagian materi ini yang telah berada di bawah domain publik atau untuk penggunaan yang diizinkan di bawah pengecualian atau pembatasan.

Tidak ada jaminan yang diberikan oleh lisensi ini. Lisensi ini mungkin tidak memberikan izin yang sesuai dengan tujuan penggunaan Anda. Sebagai contoh, hak-hak lainnya seperti hak atas potret, hak atas privasi, atau hak moral dapat membatasi penggunaan materi berlisensi CC.