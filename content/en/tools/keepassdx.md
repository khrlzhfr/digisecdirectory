---
title: KeePassDX
website: https://www.keepassdx.com/
cover: /files/keepassdx.jpg
tags:
  - Documentation & Data Management
categories:
  - Digital Security Tools
  - Password Manager
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T17:42:23.612Z
---
KeePassDX is an open-source secure password manager for Android mobile devices.