---
title: Proton Mail
website: https://proton.me/mail
cover: /files/proton-mail.jpg
flags:
  - Freemium
tags:
  - Communications & Messaging
  - Web App
categories:
  - Digital Security Tools
  - OpenPGP Encryption
  - Email
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T10:40:05.818Z
---
Proton Mail (Web, Android, iOS) is an open-source email service that uses end-to-end encryption and zero-access encryption to protect the user’s data and privacy.