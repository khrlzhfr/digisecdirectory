---
title: Bitmask VPN
website: https://bitmask.net/
cover: /files/bitmask.jpg
tags:
  - Circumvention & Anonymity
categories:
  - Digital Security Tools
  - Virtual Private Network
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T07:27:56.488Z
---
Bitmask VPN (Android, Desktop) is an open-source virtual private network (VPN).